<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
    	$this->data["a"]=1;
    	return view("web/home",$this->data);
    }
}
