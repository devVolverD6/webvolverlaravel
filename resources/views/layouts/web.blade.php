<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" />

    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script> 
    <script src="{{ asset('js/jarallax.min.js') }}"></script>
    <script src="{{ asset('js/jarallax-video.min.js') }}"></script>
</head>
<body id="@yield('page')" class="{{dispositivo()}}">
    <header class="container-fluid">
            <input id="burger" type="checkbox"/>
            <label for="burger"><i class="icon-view-menu"></i></label>
            <a href="{{url('')}}"><h1><img src="{{ asset('img/logo-volverd6.png') }}"></h1></a>
            <nav>    
                <ul>
                    <li><a href="abouts">¿Por qué <strong>VOLVER</strong>?</a></li>
                    <li><a href="trabajos">VOLVER-ADS</a></li>
                    <li><a href="volverianos">VOLVERIANOS</a></li>
                    <li><a href="clientes">VOLVER-LOVERS</a></li>
                    <li><a href="contacto">CONTACTO</a></li>
                    <li>
                        <div class="social">
                            <p>Síguenos en &nbsp;&nbsp; 
                                <a href="https://www.facebook.com/volverd6/" target="_blank"><i class="fa fa-facebook"></i></a>&nbsp;&nbsp;
                                <a href="https://pe.linkedin.com/company/volver-d6" target="_blank"><i class="fa fa-linkedin-square"></i></a>&nbsp;&nbsp;
                                <a href="https://www.youtube.com/user/VolverD6" target="_blank"><i class="fa fa-youtube-play"></i></a>
                            </p>
                      </div>
                    </li>
                </ul>
            </nav>
    </header>
    @yield('content')

<div class="modal bd-example-modal-lg" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>-->
    </div>
  </div>
</div>
    
    
    <footer class="container-fluid">
      <p>Copyright © 2018 Volver D6 / Todos los derechos reservados.</p>
    </footer>
</body>
    @yield('script_footer')
</html>