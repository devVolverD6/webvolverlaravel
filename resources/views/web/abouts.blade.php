@extends('layouts.web')
@section('title','Abouts | VolverD6')
@section('page','abouts')


@section('content')
<section class="banner">
    <img src="{{ asset('img/aboutus.jpg')}}">
</section>
<section class="content container">
    <h2><strong>VOLVAMOS</strong> A DONDE FUIMOS FELICES</h2>
    <p>Hemos descubierto el secreto del éxito de cualquier empresa y estamos dispuestos a compartirlo contigo: hay que VOLVER a unir de manera brillante la estrategia y la creatividad, dicho de otra forma: VOLVER a creer en la publicidad.</p>
    <p>Esta agencia se llama VOLVER porque quiere que sus clientes y los clientes de sus clientes, VUELVAN a esa versión suya mucho más feliz, que quizá dejaron por ahí olvidada, curiosamente ninguno de los trabajadores de VOLVER nunca dejó de soñar, de jugar, de reír y por tanto no necesitan VOLVER a ser felices porque nunca dejaron de serlo...</p>
</section>
@endsection