@extends('layouts.web')
@section('title','Clientes | VolverD6')
@section('page','clientes')


@section('content')
<section class="banner">
            <img src="{{ asset('img/clientes.jpg')}}">
        </section>
        <section class="content container">
            <h2><strong>VolverLovers</strong></h2>
            <p>Hacemos que siempre <strong>VUELVAN</strong> a sus oficinas y casas contentos de ver crecer sus marcas</p>
            <section class="logos row">
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-banco.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-mitsubishi.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-huawei.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-redbus.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-openplaza.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-segurosfalabella.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-tuenti.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-hude.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-fuso.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-bago.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-camara.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-america.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-bbva.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-fiat.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-atv.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-converse.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-bcp.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-tacama.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-big-cola.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-mg.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-semanaeconomica.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-caretas.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-sporting.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-latina.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-belisario.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-tedx.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-alfa.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-jjc.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-marcaperu.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-acuario.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-rpp.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-ipp.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-ayuda.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-bionaire.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-papachos.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-crepier.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-mistura.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-airport.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-otto.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-papajohns.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-petroperu.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-segurosimple.png')}}"></figure>
                </article>
                <!--<article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-simple.png')}}"></figure>
                </article>-->
                <!--<article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-embajada.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-bosch.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-casamor.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-cicr.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-lcperu.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-movilfalabella.png')}}"></figure>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-3 col-xl-2">
                    <figure><img src="{{ asset('img/clientes/gris/logo-sanfernando.png')}}"></figure>
                </article>-->

            </section>
        </section>
@endsection