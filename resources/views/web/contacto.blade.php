@extends('layouts.web')
@section('title','Contacto | VolverD6')
@section('page','contacto')


@section('content')
<section id="map">
            
        </section>
        <section class="content container">
            <h2><strong>CONTACTO</strong></h2>
            <p><strong>Encuéntranos : Roca y Boloña 146 - Miraflores</strong></p>
            <p><strong>Contáctanos : <a href="mailto:volverd6@volverd6.com">volverd6@volverd6.com</a> / 500-0680</strong></p>
            <p><strong>Síguenos:</strong>
                <a href="https://www.facebook.com/volverd6/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://pe.linkedin.com/company/volver-d6" target="_blank"><i class="fa fa-linkedin-square"></i></a>
                <a href="https://www.youtube.com/user/VolverD6" target="_blank"><i class="fa fa-youtube-play"></i></a>
            </p>
        </section>
@endsection
@section('script_footer')
<script>
          function initMap() {
            var uluru = {lat: -12.122254, lng: -77.0199261};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 17,
                styles: [{
                    "elementType": "geometry",
                    "stylers": [{"color": "#f5f5f5"}]
                  },
                  {
                    "elementType": "labels.icon",
                    "stylers": [{"visibility": "off"}]
                  },
                  {
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#616161"}]
                  },
                  {
                    "elementType": "labels.text.stroke",
                    "stylers": [{"color": "#f5f5f5"} ]
                  },
                  {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#bdbdbd"}]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{"color": "#eeeeee"}]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#757575"}]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [ {"color": "#e5e5e5"}]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#9e9e9e"}]
                  },
                  {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{"color": "#ffffff"}]
                  },
                  {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#757575"}]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [{"color": "#dadada"}]
                  },
                  {
                    "featureType": "road.highway",
                      
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#616161"}]
                  },
                  {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#9e9e9e"}]
                  },
                  {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [ {"color": "#e5e5e5"}
                    ]
                  },
                  {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [{"color": "#eeeeee"}]
                  },
                  {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{"color": "#c9c9c9"}]
                  },
                  {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#9e9e9e"}]
                  }
                ],
              center: uluru
            });
              
              var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">Volver D6</h1>'+
      '<div id="bodyContent">'+
      '<p><strong>Encuéntranos : Roca y Boloña 146 - Miraflores</strong></p>'+
      '<p><strong>Contáctanos : <a href="mailto:volverd6@volverd6.com">volverd6@volverd6.com</a> / 500-0680</strong></p>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
              
            var marker = new google.maps.Marker({
    position: uluru,
    map: map,
    title: 'Uluru (Ayers Rock)'
  });
              
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
infowindow.open(map, marker);
              
          }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrlYB29tuGlS5nfZGgFHyhKTor_DxlQK8&callback=initMap">
        </script>
@endsection