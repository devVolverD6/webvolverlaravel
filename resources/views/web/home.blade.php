@extends('layouts.web')
@section('title','Home | VolverD6')
@section('page','home')

@section('content')
<section class="video clearfix">
         @if (dispositivo()=="desktop")
        <div class="jarallax" data-video-src="https://www.youtube.com/embed/mBw9BnkaW7E" data-jarallax='{"videoStartTime": 10, "videoEndTime": 15}'>
        </div>
        @else
        <figure> <img src="{{ asset('img/home/principal.jpg')}}"></figure>
        @endif
        <div class="cover">
            <div class="container intro"><p>En este mundo hace falta hacer las cosas diferentes y <strong>VOLVER</strong> a ser feliz. Hace falta <strong>VOLVER…</strong></p></div>
        </div>
    </section>
    <section class="jobs clearfix">
        <article class="col-md-6 vid">
            <a href="#" data-type="video" data-url="https://www.youtube.com/embed/dQRyv_oLOMU" data-title='“14 Selfies para este 14”'>
                @if (dispositivo()=="desktop")
                <div class="jarallax" data-video-src="https://www.youtube.com/embed/dQRyv_oLOMU" data-jarallax='{"videoStartTime": 15, "videoEndTime": 19}'>
                </div>
                @else
                <figure><img src="{{ asset('img/home/huawei.jpg')}}"></figure>
                @endif
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left"><img src="{{ asset('img/clientes/blanco/huawei.png')}}"><br>
                            <h3>“14 Selfies para este 14”</h3>
                        </div> 
                        <div class="right"></div>
                    </div>
                </figcaption>
            </a>
        </article>
        <article class="col-md-6 vid">
            <a href="#" data-type="video" data-url="https://www.youtube.com/embed/fOEvMxJAs3U" data-title='“Sorry pá”'>
                @if (dispositivo()=="desktop")
                <div class="jarallax" data-video-src="https://www.youtube.com/embed/fOEvMxJAs3U" data-jarallax='{"videoStartTime": 10, "videoEndTime": 15}'>
                </div>
                @else
                <figure><img src="{{ asset('img/home/sorrypa.jpg')}}"></figure>
                @endif
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}"><br>
                            <h3>“Sorry pá”</h3>
                        </div>
                        <div class="right"></div>
                    </div>
                </figcaption>
            </a>
        </article>
        <article class="col-md-12 vid full">
            <a href="#" data-type="video" data-url="https://www.youtube.com/embed/tzLDSSftpcI" data-title='“VIAJEROS FANTÁSTICOS – TEMPORADA 1”'>
                @if (dispositivo()=="desktop")
                <div class="jarallax" data-video-src="https://www.youtube.com/embed/tzLDSSftpcI" data-jarallax='{"videoStartTime": 18, "videoEndTime": 23}'>
                </div>
                @else
                <figure><img src="{{ asset('img/home/viajeros.jpg')}}"></figure>
                @endif
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left"><img src="{{ asset('img/clientes/blanco/redbus.png')}}"><br>
                            <h3>“VIAJEROS FANTÁSTICOS – TEMPORADA 1”</h3>
                        </div>
                        <div class="right"></div>
                    </div>
                </figcaption>
            </a>
        </article>
        <article class="col-md-6 vid">
            <a href="#" data-type="video" data-url="https://www.youtube.com/embed/LMR-bGTnkOU" data-title='“PROYECTO TUINS”'>
                @if (dispositivo()=="desktop")
                <div class="jarallax" data-video-src="https://www.youtube.com/embed/LMR-bGTnkOU" data-jarallax='{"videoStartTime": 18, "videoEndTime": 23}'>
                </div>
                @else
                <figure><img src="{{ asset('img/home/tueins.jpg')}}"></figure>
                @endif
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left">
                            <img src="{{ asset('img/clientes/blanco/tuenti.png')}}"><br>
                            <h3>“PROYECTO TUINS”</h3>
                        </div>
                        <div class="right"></div>
                    </div>
                </figcaption>
            </a>
        </article>
        <article class="col-md-6 vid">
            <a href="#" data-type="video" data-url="https://www.youtube.com/embed/jZkdoZl2EjY" data-title='“MÁS QUE UN VOUCHER”'>
                @if (dispositivo()=="desktop")
                <div class="jarallax" data-video-src="https://www.youtube.com/embed/jZkdoZl2EjY" data-jarallax='{"videoStartTime": 10, "videoEndTime": 15}'>
                </div>
                @else
                <figure><img src="{{ asset('img/home/vouchers.jpg')}}"></figure>
                @endif
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left"><img src="{{ asset('img/clientes/blanco/open-plaza.png')}}"><br>
                            <h3>“MÁS QUE UN VOUCHER”</h3>
                        </div>
                        <div class="right"></div>
                    </div>
                </figcaption>
            </a>
        </article>
        <article class="col-md-12 vid full">
            <a href="#" data-type="video" data-url="https://www.youtube.com/embed/fhn-LtLpj_w" data-title='“HOMBRE EN UN CAJERO”'>
                @if (dispositivo()=="desktop")
                <div class="jarallax" data-video-src="https://www.youtube.com/embed/fhn-LtLpj_w" data-jarallax='{"videoStartTime": 60, "videoEndTime": 65,"videoPlayOnlyVisible":false}'>
                </div>
                @else
                <figure><img src="{{ asset('img/home/hombrecajero.jpg')}}"></figure>
                @endif
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left"><img src="{{ asset('img/clientes/blanco/fundacion_bbva.png')}}"><br>
                            <h3>“HOMBRE EN UN CAJERO”</h3>
                        </div>
                        <div class="right"></div>
                    </div>
                </figcaption>
            </a>
        </article>
        <article class="col-md-6 img">
            <a href="#" data-type="img" data-url="img/impostergable.jpg" data-title='“PRECIO IMPOSTERGABLE – ARQUERO”'>
                @if (dispositivo()=="desktop")
                <figure style="background-image: url(img/impostergable.jpg)" class="parallax"></figure>
                </div>
                @else
                <figure><img src="{{ asset('img/impostergable.jpg')}}"></figure>
                @endif
                
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left"><img src="{{ asset('img/clientes/blanco/seguros-falabella.png')}}"><br>
                            <h3>“PRECIO IMPOSTERGABLE – ARQUERO”</h3>
                        </div>
                        <div class="right"></div>
                      </div>
                </figcaption>
            </a>
        </article>
        <article class="col-md-6 vid">
            <a href="#" data-type="video" data-url="https://www.youtube.com/embed/Q2GOQ6_iWfk" data-title='“EL MEJOR DÍA DEL PADRE”'>
                @if (dispositivo()=="desktop")
                <div class="jarallax" data-video-src="https://www.youtube.com/embed/Q2GOQ6_iWfk" data-jarallax='{"videoStartTime": 15, "videoEndTime": 19}'>
                </div>
                @else
                <figure><img src="{{ asset('img/home/fuso.jpg')}}"></figure>
                @endif
                <figcaption>
                    <div class="desLeft">
                        <div class="over"></div>
                        <div class="left"><img src="{{ asset('img/clientes/blanco/fuso.png')}}"><br>
                            <h3>“EL MEJOR DÍA DEL PADRE”</h3>
                        </div>
                        <div class="right"></div>
                    </div>
                </figcaption>
            </a>
        </article>
    </section>
@endsection

@section('script_footer')
    <script type="text/javascript">
        //objectFitImages();
        jarallax(document.querySelectorAll('.jarallax'));
        
        /*$('.over').hover(function() {
            $(this).firstChild().next().firstChild().parent().prev().addClass('transition');
        }, function() {
            $(this).parent().parent().prev().removeClass('transition');
        });*/
    </script>
    <script type="text/javascript">
        $("article a").click(function(){
            var element
            var url=$(this).attr("data-url");
            var title=$(this).attr("data-title");
            if($(this).attr("data-type")=="video"){
                    element='<iframe width="100%" height="500" src="'+url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
            }else{
                element='<img src="'+url+'">';
            }
            $("#myModal .modal-title").html(title);
            $("#myModal .modal-body").html(element);
            $('#myModal').modal('show')
        })
        $('#myModal').on('hidden.bs.modal', function (e) {
            $("#myModal .modal-title").html(" ");
            $("#myModal .modal-body").html(" ");
        })
    </script>
@endsection
