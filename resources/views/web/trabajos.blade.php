@extends('layouts.web')
@section('title','Trabajos | VolverD6')
@section('page','trabajos')

@section('content')
<section class="banner">
            <img src="{{ asset('img/trabajos.jpg')}}">
        </section>
        <section class="content container">
            <h2><strong>VOLVER ADS</strong></h2>
            <p>Nos amanecemos haciéndolo posible y luego nos <strong>VUELVE</strong> a quitar el sueño de lo orgullosos que estamos</p>
            
        </section>
        <section class="jobs container">
            <div class="row">
                <article data-type="video" data-url="https://www.youtube.com/embed/GMhl7rCRFNk" data-title="redBus - CyberBus" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo4.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/tzLDSSftpcI" data-title="Redbus v1 1min" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo2.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/d9KMr-ZnLqw" data-title="redBus - Intro Gary" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo5.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/4viyhtH_rAk" data-title='REDBUS "El amor en tiempos de viaje"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo11.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/ZAjVqUOEl4k" data-title='REDBUS "Ruta y rutinas"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo12.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/BzyGIbI2Bg4" data-title='REDBUS "El amanecer de las terramozas"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo15.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/1NSNUKoX13k" data-title='REDBUS "Buses, cámara y fama"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo18.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/i-NYwewCmgY" data-title='REDBUS "20% Lucho"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo19.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/tsJPxe0INTQ" data-title='REDBUS "20%"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo23.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/DYa4cUtIjIA" data-title='REDBUS - "20% en julio"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo24.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/SZeiMWvMjiw" data-title='REDBUS -"Dracula"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo25.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/WZm2TJx6fAQ" data-title='REDBUS - "Lobo"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo26.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/redbus.png')}}" ></figure>
                </article>
                
                
                
                
                <article data-type="video" data-url="https://www.youtube.com/embed/nPer3wno2SY" data-title='BANCO FALABELLA CMR "Día de la Madre"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo29.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/fOEvMxJAs3U" data-title='BANCO FALABELLA "CMR 100 000 puntos"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo31.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/IqmxjPG4aoc" data-title="BANCO FALABELLA CMR Visa Paga" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo33.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/oLJrIsNiG9I" data-title='BANCO FALABELLA "Oportunidad única" Día del Padre' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo43.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/-1IB6VId05g" data-title='BANCO FALABELLA "Oportunidad Única" Vuelta al cole' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo46.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/JIxif7ohhvg" data-title='BANCO FALABELLA CMR VISA "Doble felicidad"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo47.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/qmzNREbho8E" data-title='BANCO FALABELLA Pizza Hut "Yerno"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo48.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/Wf2Oee3UasQ" data-title='BANCO FALABELLA Cuenta Sueldo 2015' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo56.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                
                
                <article data-type="video" data-url="https://www.youtube.com/embed/1IX_93Tw7mU" data-title="BANCO FALABELLA Y AMERICA SOLIDARIA" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo34.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/foAA2RDM2fY" data-title='BANCO FALABELLA & AMÉRICA SOLIDARIA "¡La pobreza desapareció!"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo20.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/logo-customer.png')}}" ></figure>
                </article>
                
                
                
                

                <article data-type="video" data-url="https://www.youtube.com/embed/FWIlvoLZxlw" data-title='TUENTI App "Filtros de Voz"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo9.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/qQvGUFuYBpQ" data-title='TUENTI App "Combos Personalizables"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo10.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/4_wP_Rd9li4" data-title='TUENTI "Método Tay Peca"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo13.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/w_HaW8tsXxo" data-title='TUENTI App "Método Fotoclonadora"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo14.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/hz8iERZnqbk" data-title='TUENTI App "Método Igualatex"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo16.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/LMR-bGTnkOU" data-title='Tuenti "Proyecto Tuins"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo17.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/ck6l-eyt0WI" data-title="TUENTI HALOWEEN" class="col-6 col-sm-4 col-md-3">
                    <figure><img src="img/jobs/trabajo1.jpg" class="zoom"></figure>
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/PhyuY2E-hsc" data-title='TUENTI App "Recarga"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo6.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/YD33OAOIjGs" data-title='TUENTI "Halloween 2017"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo7.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/FyIHfqU8YqM" data-title='TUENTI App "Multiplataforma"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo8.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/oLZxqfl2J80" data-title='TUENTI "Tienda' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo21.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/7aX9Xir3BJQ" data-title='TUENTI "Conferencia"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo22.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/7dakcMLz1xY" data-title='TUENTI Dia de la Madre "Ahitapper"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo30.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/-7bMR4ORrW4" data-title="TUENTI Radio" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo32.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/np345EUVYDo" data-title='TUENTI "Activación"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo35.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/oZrLIKPzOgs" data-title='TUENTI "Té de tías"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo36.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/OD2pxmYdnFw" data-title='TUENTI "Parilla"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo37.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/BPftSe4vh9g" data-title='TUENTI "¿Tienes Tuenti o cuarenti?"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo38.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/qunvlsiyaF8" data-title='TUENTI DECISIONES' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo49.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/tuenti.png')}}" ></figure>
                </article>
                
                <article data-type="video" data-url="https://www.youtube.com/embed/mBw9BnkaW7E" data-title='LABORATORIOS BAGO - NASTIZOL FORTE "Carrera"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo28.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/bago.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/5IJ705lqP9g" data-title='LABORATORIOS BAGÓ. NASTIZOL "Estornudo"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo50.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/bago.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/saPZGdLQY-E" data-title='LABORATORIOS BAGÓ. NASTIZOL "Nariz como caño"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo51.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/bago.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/Z5UEEzfiTSQ" data-title='LABORATORIOS BAGÓ. NASTIZOL "Cuerpo cortado"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo52.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/bago.png')}}" ></figure>
                </article>
                

                <article data-type="video" data-url="https://www.youtube.com/embed/FklWb7stqO4" data-title='HUDE Radio "Hechos el uno para el otro"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo39.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/hude.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/pzJKhP7Lmxo" data-title="HUDE Escobestia" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo40.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/hude.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/Uuy2hIPands" data-title="HUDE Escobina" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo41.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/hude.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/fFaVYAj4mXU" data-title="HUDE Escobón" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo42.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/hude.png')}}" ></figure>
                </article>
                
                
                <article data-type="video" data-url="https://www.youtube.com/embed/S_ptbGAQLyo" data-title='SEMANA ECONÓMICA Radio "Jóvenes"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo53.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/semana-economica.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/h5FsyAAgrSA" data-title='SEMANA ECONÓMICA Radio "Exportas"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo54.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/semana-economica.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/sa8wPFHVbA0" data-title="SEMANA ECONÓMICA" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo55.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/semana-economica.png')}}" ></figure>
                </article>
                
                
                <article data-type="video" data-url="https://www.youtube.com/embed/K4-bYl2KUuY" data-title='LATINA "Fútbol"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo44.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/latina.png')}}" ></figure>
                </article>
                <article data-type="video" data-url="https://www.youtube.com/embed/Q8qLK9VB4Sk" data-title='LATINA "Caída"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo45.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/latina.png')}}" ></figure>
                </article>

                <article data-type="video" data-url="https://www.youtube.com/embed/Q2GOQ6_iWfk" data-title='FUSO - "El mejor Día del Padre"' class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo27.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/fuso.png')}}" ></figure>
                </article>
                
                <article data-type="video" data-url="https://www.youtube.com/embed/jZkdoZl2EjY" data-title="MIÉRCOLES DE CMR - OPEN PLAZA" class="col-6 col-sm-4 col-md-3">
                    <img src="img/jobs/trabajo3.jpg" class="zoom">
                    <div class="play"><img src="{{ asset('img/play.png')}}"></div>
                    <figure class="logo"><img src="{{ asset('img/clientes/blanco/open-plaza.png')}}" ></figure>
                </article>
            </div>
        </section>
        <section class="content container">
            <p>¿Querías ver más? Por favor, Vuelve pronto, son tantos los trabajos que no dejamos de subir y subir más cosas, o mejor aún: contáctanos para empezar a revolucionar tu marca ¡ya mismo!</p>
        </section>
@endsection



@section('script_footer')
<script type="text/javascript">
        $('.zoom').hover(function() {
            $(this).addClass('transition');
        }, function() {
            $(this).removeClass('transition');
        });
        $('.play').hover(function() {
            $(this).prev().addClass('transition');
        });
        
        $(".jobs article").click(function(){
            var element
            var url=$(this).attr("data-url");
            if($(this).attr("data-type")=="video"){
                element='<iframe width="100%" height="400" src="'+url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
            }else{
                element='<img src="'+url+'">';
            }
            $("#myModal .modal-title").html($(this).attr("data-title"));
            $("#myModal .modal-body").html(element);
            $('#myModal').modal('show')
        })
        $('#myModal').on('hidden.bs.modal', function (e) {
            $("#myModal .modal-title").html(" ");
            $("#myModal .modal-body").html(" ");
        })
    </script>
@endsection