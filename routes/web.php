<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'web\HomeController@index');
Route::get('/abouts', 'web\AboutsController@index');
Route::get('/trabajos', 'web\TrabajosController@index');
Route::get('/volverianos', 'web\VolverianosController@index');
Route::get('/clientes', 'web\ClientesController@index');
Route::get('/contacto', 'web\ContactoController@index');
